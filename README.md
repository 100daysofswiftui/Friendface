# Friendface

Friendface is an app that display example users which are fetched from a JSON file from the internet.

This app was developed as a challenge from the [100 Days of SwiftUI](https://www.hackingwithswift.com/guide/ios-swiftui/5/3/challenge) course from Hacking with Swift.
